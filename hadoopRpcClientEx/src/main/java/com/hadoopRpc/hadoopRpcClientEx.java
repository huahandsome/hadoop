package com.hadoopRpc;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.net.InetSocketAddress;

/**
 * Created by huahandsome on 7/5/17.
 */
public class hadoopRpcClientEx {
    public static void main( String[] args ) {
        InetSocketAddress addr = new InetSocketAddress("debian", 12345);
        Configuration conf = new Configuration();

        try {
            LoginServiceInterface proxy = RPC.getProxy( LoginServiceInterface.class, 100L, addr, conf);
            //LoginServiceInterface proxy = RPC.getProxy( LoginServiceInterface.class, LoginServiceInterface.versionID, addr, conf);
            String ret = proxy.login( "Helloworld", "123456" );
            System.out.println( ret );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
