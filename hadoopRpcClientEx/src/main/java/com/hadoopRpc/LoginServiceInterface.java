/**
 * Created by huahandsome on 7/5/17.
 */
package com.hadoopRpc;
public interface LoginServiceInterface {
    public static final long versionID = 1L;
    public String login( String username, String password );
}
