package org.apache.mr.splitwords;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by huahandsome on 7/12/17.
 */
// four type for mapper: KEYIN, VALUEIN, KEYOUT, VELUEOUT
// KEYIN : input data type,  VELUEIN : input value type
// KEYOUT: output data type, VALUEOUT: output value type

public class WCMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String formattedLine = formatString(line);
        String[] words = splitString(formattedLine);

        for( String word: words ) {
            context.write( new Text(word), new LongWritable(1) );
        }
    }

    private String formatString(String inStr){
        String outStr = null;
        String delim = "),(";

        String[] inStrSplittedWithTab = inStr.split("\t");
        outStr = inStrSplittedWithTab[0] + ",";

        String strTuple = inStrSplittedWithTab[1].substring(5, inStrSplittedWithTab[1].length()-3);

        StringTokenizer st = new StringTokenizer(strTuple, delim, false);

        while(st.hasMoreTokens()) {
            outStr += st.nextToken() + ",";
        }

        return outStr.substring(0, outStr.length()-1);
    }

    private String[] splitString(String inStr){
        String[] strArray = inStr.split(",");
        int arrLength = strArray.length;
        String[] outArray = new String[arrLength/4];
        int i = 0;
        int j = 1;

        for (; i < arrLength/4; j=j+4,i++){
            outArray[i] = strArray[0] + "," + strArray[j] + "," + strArray[j+1] + "," + strArray[j+2] + ","+ strArray[j+3];
        }

        return outArray;
    }
}
