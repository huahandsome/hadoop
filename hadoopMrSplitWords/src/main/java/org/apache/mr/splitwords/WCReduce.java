package org.apache.mr.splitwords;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by huahandsome on 7/12/17.
 */
public class WCReduce extends Reducer<Text, LongWritable, Text, LongWritable> {

    // when map complete, it will buffer all key-value pair,
    // then pass a key-value array [hello, 1, 1, 1, 1, 1] and call reduce
    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
       context.write(key, null);
    }
}
