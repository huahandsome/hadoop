package com.hadoopRpc;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.RPC.Builder;
import org.apache.hadoop.ipc.Server;
//import org.apache.hadoop.io.Text;
import java.io.IOException;

/**
 * Created by huahandsome on 7/5/17.
 */
public class Starter {
    public static void main( String args[] ) {
        Builder builder= new RPC.Builder( new Configuration() );
        builder.setBindAddress("debian").setPort(12345).setProtocol(LoginServiceInterface.class).setInstance( new LoginServiceImpl());

        try {
            Server server = builder.build();
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
