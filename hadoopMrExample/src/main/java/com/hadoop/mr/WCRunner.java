package com.hadoop.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Created by huahandsome on 7/12/17.
 */
public class WCRunner {
    public static void main ( String[] args ) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance( conf );

        // set classpath for the jar
        job.setJarByClass( WCRunner.class );

        job.setMapperClass( WCMapper.class );
        job.setReducerClass( WCReduce.class );

        job.setOutputKeyClass( Text.class );
        job.setOutputValueClass( LongWritable.class );

        job.setMapOutputKeyClass( Text.class );
        job.setMapOutputValueClass( LongWritable.class );

        FileInputFormat.setInputPaths( job, new Path("/wc/srcdata") );
        FileOutputFormat.setOutputPath( job, new Path("/wc/output") );

        job.waitForCompletion(true ) ;
    }
}
