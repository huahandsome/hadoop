package com.hadoop.mr;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.util.StringUtils;
import java.io.IOException;

/**
 * Created by huahandsome on 7/12/17.
 */
// four type for mapper: KEYIN, VALUEIN, KEYOUT, VELUEOUT
// KEYIN : input data type,  VELUEIN : input value type
// KEYOUT: output data type, VALUEOUT: output value type

public class WCMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String words[] = StringUtils.split( line, ' ' );

        for( String word: words ) {
            context.write( new Text(word), new LongWritable(1) );
        }
    }
}
