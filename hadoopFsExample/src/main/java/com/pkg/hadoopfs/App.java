package com.pkg.hadoopfs;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//import org.junit.Test;
/**
 * Hello world!
 *
 */

public class App {
    static FileSystem fs = null;

    public static void main( String[] args ) throws IOException {

        // download from hdfs
        //Configuration conf = new Configuration();
        //conf.set("fs.defaultFS", "hdfs://debian:9000");
        //FileSystem fs = FileSystem.get( conf );

        //Path src = new Path("/CRF.pdf");
        //FSDataInputStream in = fs.open(src);

        //FileOutputStream out = new FileOutputStream("/home/huahandsome/CRFDownload.pdf");
        //IOUtils.copy(in, out);
        init();

        try {
            //upload();
            //download();
            //mkDir();
            //rmDir();
            list();
        } catch( Exception e ) {

        }
    }

    //@Before
    public static void init() {
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://debian:9000");

        try {
            fs = FileSystem.get(conf);
        } catch ( Exception e ) {

        }
    }

    //upload to hdfs
    //@Test
    public static void upload() {
        //Configuration conf = new Configuration();
        //conf.set("fs.defaultFS", "hdfs://debian:9000");

        //FileSystem fs = FileSystem.get(conf);

        //Path dst = new Path("/uploadFile");

        //FSDataOutputStream out = fs.create(dst);

        //FileInputStream in = new FileInputStream("/home/huahandsome/test.file");

        //IOUtils.copy(in, out);
        Path src = new Path("/home/huahandsome/test.file");
        Path dst = new Path( "/uploadFile1");

        try {
            fs.copyFromLocalFile( src, dst );
        } catch ( Exception e ) {

        }
    }

    //download from hdfs
    public static void download() {
        Path dst = new Path("/home/huahandsome/downloadFile");
        Path src = new Path( "/uploadFile1");

        try {
            fs.copyToLocalFile( src, dst );
        } catch ( Exception e ) {

        }
    }

    // list file in hdfs
    public static void list() {
        try {
            // listFiles() only shows files, not include folders
            RemoteIterator<LocatedFileStatus> files = fs.listFiles( new Path("/"), true );
            while( files.hasNext() ) {
                LocatedFileStatus file = files.next();
                Path filePath = file.getPath();
                String fileName = filePath.getName();
                System.out.println( fileName );
            }
        } catch ( Exception e ) {

        }

        System.out.println("-----------------------");
        try {
            // listStatus() shows files and folders
            FileStatus[] listStatus = fs.listStatus(new Path("/"));
            for (FileStatus status : listStatus) {
                String fileName = status.getPath().getName();
                System.out.println(fileName);
            }
        }catch( Exception e ){

        }
    }

    // create directory
    public static void mkDir() {
        try {
            fs.mkdirs( new Path("/testMkdir/testSubDir") );
        } catch ( Exception e ) {

        }
    }

    // delete directory
    public static void rmDir() {
        try {
            fs.delete( new Path("/testMkdir"), true );
        } catch ( Exception e ) {

        }
    }
}
