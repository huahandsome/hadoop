package org.apache.stormDemo;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.TopologyBuilder;

/**
 * Created by huahandsome on 7/19/17.
 */
public class TopoMain {
    public static void main(String[] args) throws InvalidTopologyException, AuthorizationException, AlreadyAliveException {
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("randomspout", new RandomWordSpout(), 4);
        builder.setBolt("upperBolt", new UpperBolt(), 4).shuffleGrouping("randomspout");
        builder.setBolt("suffixBolt", new SuffixBolt(), 4).shuffleGrouping("upperBolt");

        StormTopology topology = builder.createTopology();

        Config conf = new Config();
        conf.setNumWorkers(4);
        conf.setDebug(true);
        conf.setNumAckers(0);

        StormSubmitter.submitTopology("demoTopo", conf, topology);
    }
}
