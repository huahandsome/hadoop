package org.apache.stormDemo;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.util.Map;
import java.util.Random;

/**
 * Created by huahandsome on 7/19/17.
 */
public class RandomWordSpout extends BaseRichSpout {

    String[] words = { "iphone", "xiaomi", "mate", "sony", "sumsung", "moto", "meizu" };
    private SpoutOutputCollector collector;

    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        collector = spoutOutputCollector;
    }


    public void nextTuple() {
        Random random = new Random();
        int idx = random.nextInt( words.length );

        String word = words[idx];

        collector.emit(new Values(word));

        Utils.sleep(2000);
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare( new Fields("orignname"));
    }
}
