package org.apache.stormDemo;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Created by huahandsome on 7/19/17.
 */
public class SuffixBolt extends BaseBasicBolt {
    FileWriter fileWriter = null;

    public void prepare(Map stormConf, TopologyContext context) {
        try {
            fileWriter = new FileWriter("/home/huahandsome/" + UUID.randomUUID() );
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        String word = tuple.getString(0);
        String suffix_word = word + "_____";

        try {
            fileWriter.write(suffix_word);
            fileWriter.write("\n");
            fileWriter.flush();
        }catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
